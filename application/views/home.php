<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view('_partials/head.php'); ?>
</head>

<body>
<?php $this->load->view('_partials/navbar.php'); ?>

	<h1>Home Page</h1>

	<?php $this->load->view('_partials/footer.php'); ?>
</body>

</html>